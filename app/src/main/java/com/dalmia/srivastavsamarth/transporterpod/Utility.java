package com.dalmia.srivastavsamarth.transporterpod;

/**
 * Created by Srivastav.Samarth on 6/16/2017.
 */

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Utility {
    private static Pattern pattern;
    private static Matcher matcher;

    private static final String NO_PATTERN="^[789]\\d{9}$";
    public static boolean validateno(String t_code)
    {
        pattern=Pattern.compile(NO_PATTERN);
        matcher=pattern.matcher(t_code);
        return matcher.matches();
    }

    public static boolean isNotNull(String txt){
        return (txt!=null && txt.trim().length()>0);
    }

}