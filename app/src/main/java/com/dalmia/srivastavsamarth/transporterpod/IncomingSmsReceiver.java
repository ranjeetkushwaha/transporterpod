package com.dalmia.srivastavsamarth.transporterpod;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Kushwaha.Ranjeet on 5/9/2017.
 */


public class IncomingSmsReceiver extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
        SmsMessage [] messages = null;
        String strMessage = "";

        if (bundle != null)
        {
            Object [] pdus = (Object[]) bundle.get("pdus");

            messages = new SmsMessage[pdus.length];

            for (int i = 0; i < messages.length; i++)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    String format = bundle.getString("format");
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                }
                else {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                strMessage += "SMS From: " + messages[i].getOriginatingAddress();
                strMessage += " : ";
                strMessage += messages[i].getMessageBody();
                strMessage += "\n";

                String phoneNumber = messages[i].getOriginatingAddress();
                Log.i("SmsReceiver", "senderNum: " + strMessage);

                if(phoneNumber.trim().contains("DALMIA")) {
                    String senderNum = phoneNumber;
                    String message = messages[i].getMessageBody();

                    String number = message.substring(message.lastIndexOf(" ") + 1);
                    System.out.println(senderNum + ", " + number + ", " + message);

                    Intent myIntent = new Intent("otp");
                    myIntent.putExtra("message", number);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);

                }
            }

            Log.e("SMS", strMessage);
            //Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
        }

       /* System.out.println("onReceive" + ", " + bundle);
        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    if(phoneNumber.trim().contains("DALMIA")) {
                        String senderNum = phoneNumber;
                        String message = currentMessage.getDisplayMessageBody();

                        String number = message.substring(message.lastIndexOf(" ") + 1);
                        System.out.println(senderNum + ", " + number + ", " + message);
                        Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                        Intent myIntent = new Intent("otp");
                        myIntent.putExtra("message", number);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);

                    }


                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }*/
    }
}
