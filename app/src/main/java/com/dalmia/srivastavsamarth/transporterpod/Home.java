package com.dalmia.srivastavsamarth.transporterpod;


import android.content.Context;
import android.content.Intent;

import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class Home extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    int day = 1, month = 1, year = 1996;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static MyAllRecyclerViewAdapter adapter;
    private static MyItemRecyclerViewAdapter adapter1;
    private static MyItemRecyclerViewAdapter1 adapter2;
    Button btnClosePopup;
    TextView t1, t2;
    ImageButton btnCreatePopup;
    CardView cd;
    public static JSONArray jsonArray;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    int width = 50, height = 50;
    public static List<TransporterPOD> posts;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        requestJSONObject();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());




        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);




        btnCreatePopup = (ImageButton) findViewById(R.id.imageButton);
        btnCreatePopup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                initiatePopupWindow();
            }


        });

    }

    private void requestJSONObject() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://10.10.28.127:8081/TransporterPOD/login/getdata";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("Home ", "Response " + response);
                GsonBuilder builder = new GsonBuilder();
                Gson mGson = builder.create();
                TransporterPOD[] arr = mGson.fromJson(response, TransporterPOD[].class);
                posts = Arrays.asList(arr);
                Log.d("post", "size: " + posts.size() + "customer: " + posts.get(0).getCustomer());

                mViewPager.setAdapter(mSectionsPagerAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Home", "Error " + error.getMessage());
            }
        });
        queue.add(stringRequest);
    }

    public void act(View v) {
        Intent i = new Intent(Home.this, MapsActivity.class);
        startActivity(i);
    }

    private PopupWindow pwindo, pwindow, pwindow1;

    private void initiatePopupWindow() {
        try {
            LayoutInflater inflater = (LayoutInflater) Home.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.fragment_filter_option, null);
            View nv = findViewById(R.id.appbar);
            pwindo = new PopupWindow(layout, width - 400, width - 400, true);
            pwindo.setBackgroundDrawable(new ColorDrawable());
            pwindo.showAtLocation(layout, Gravity.RIGHT | Gravity.TOP, 40, nv.getHeight());
            pwindo.setElevation(50);
            btnClosePopup = (Button) layout.findViewById(R.id.submit);
            btnClosePopup.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    pwindo.dismiss();
                }
            });

            t1 = (TextView) layout.findViewById(R.id.from_date);
            t2 = (TextView) layout.findViewById(R.id.to_date);


            t1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {

                    pwindo.setOutsideTouchable(false);
                    pwindo.setFocusable(false);
                    pwindo.setTouchable(false);
                    initiatePopupWindow1();

                }


            });

            t2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    pwindo.setOutsideTouchable(false);
                    pwindo.setFocusable(false);
                    pwindo.setTouchable(false);
                    initiatePopupWindow2();
                }


            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initiatePopupWindow1() {
        try {


            LayoutInflater inflater = (LayoutInflater) Home.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.fragment_date_picker,
                    (ViewGroup) findViewById(R.id.constraint_lay));
            pwindow = new PopupWindow(layout, width - 100, height - width, true);
            pwindow.setBackgroundDrawable(new ColorDrawable());
            pwindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
            pwindow.setElevation(50);
            Button btnClosePopup2 = (Button) layout.findViewById(R.id.sub);
            View v = layout.findViewById(R.id.datePicker);
            final DatePicker datePicker = (DatePicker) v;
            Calendar c = Calendar.getInstance();
            datePicker.setMaxDate(c.getTimeInMillis());
            btnClosePopup2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindow.dismiss();
                    day = datePicker.getDayOfMonth();
                    month = datePicker.getMonth() + 1;
                    year = datePicker.getYear();
                    t1.setText(String.format("%02d/%02d/%04d", (day), (month), year));

                }

            });
            setFinishOnTouchOutside(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindow2() {
        try {
            LayoutInflater inflater = (LayoutInflater) Home.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.fragment_date_picker,
                    (ViewGroup) findViewById(R.id.constraint_lay));
            pwindow1 = new PopupWindow(layout, width - 100, height - width, true);
            pwindow1.setBackgroundDrawable(new ColorDrawable());
            pwindow1.showAtLocation(layout, Gravity.CENTER, 0, 0);
            pwindow1.setElevation(50);
            Button btnClosePopup2 = (Button) layout.findViewById(R.id.sub);
            View v1 = layout.findViewById(R.id.datePicker);
            final DatePicker datePicker = (DatePicker) v1;
            Calendar c = Calendar.getInstance();
            datePicker.setMaxDate(c.getTimeInMillis());
            btnClosePopup2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindow1.dismiss();
                    day = datePicker.getDayOfMonth();
                    month = datePicker.getMonth() + 1;
                    year = datePicker.getYear();
                    t2.setText(String.format("%02d/%02d/%04d", (day), (month), year));

                }

            });
            setFinishOnTouchOutside(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



        /**
         * A placeholder fragment containing a simple view.
         */
        public static class PlaceholderFragment extends Fragment {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            public List<TransporterPOD> ongoing_list = new ArrayList<TransporterPOD>();
            public List<TransporterPOD> completed_list = new ArrayList<TransporterPOD>();
            public List<TransporterPOD> all_list = new ArrayList<TransporterPOD>();
            private static final String ARG_SECTION_NUMBER = "section_number";

            public PlaceholderFragment() {
            }

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            public static PlaceholderFragment newInstance(int sectionNumber) {
                PlaceholderFragment fragment = new PlaceholderFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, sectionNumber);
                fragment.setArguments(args);
                return fragment;
            }


            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View rootView = inflater.inflate(R.layout.fragment_all_list, container, false);
                RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list1);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                Log.d("post","size34: "+ posts.size()+"customer: "+ posts.get(0).getCustomer());
                completed_list.clear();
                ongoing_list.clear();

                all_list=posts;
                for(int i = 0; i<posts.size();i++)
                {
                    if(posts.get(i).isStat()) {
                        //stat = true => ongoing

                            ongoing_list.add(posts.get(i));

                    }
                    else
                    {
                        //completed
                            completed_list.add(posts.get(i));

                    }
                }
                switch(getArguments().getInt(ARG_SECTION_NUMBER)) {
                    case 1:
                        adapter = new MyAllRecyclerViewAdapter(getActivity(), all_list);
                        mRecyclerView.setAdapter(adapter);
                        break;
                    case 2:
                        adapter1 = new MyItemRecyclerViewAdapter(getActivity(), ongoing_list);
                        mRecyclerView.setAdapter(adapter1);
                        break;
                    case 3:
                        adapter2 = new MyItemRecyclerViewAdapter1(getActivity(), completed_list);
                        mRecyclerView.setAdapter(adapter2);
                        break;
                }
                            return rootView;
            }
        }

        /**
         * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
         * one of the sections/tabs/pages.
         */
         class SectionsPagerAdapter extends FragmentPagerAdapter {

            public SectionsPagerAdapter(FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int position) {
                // getItem is called to instantiate the fragment for the given page.
                // Return a PlaceholderFragment (defined as a static inner class below).
                return PlaceholderFragment.newInstance(position + 1);
            }

            @Override
            public int getCount() {
                // Show 3 total pages.
                return 3;
            }
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "All";
                    case 1:
                        return "ONGOING";
                    case 2:
                        return "COMPLETED";
                }
                return null;
            }
        }

    }

