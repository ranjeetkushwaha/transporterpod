package com.dalmia.srivastavsamarth.transporterpod;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class OTPScreen extends AppCompatActivity {


    TextView t,t10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpscreen);
        t= (TextView)findViewById(R.id.transp);
        t10 = (TextView) findViewById(R.id.timer);
        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                t10.setText("time remaining: " + String.format("%02d:%02d", (millisUntilFinished/(1000*60)), (millisUntilFinished / 1000) % 60));
            }

            public void onFinish() {
                t10.setText("done!");
            }
        }.start();
    }
    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    public void verify(View v)
    {
        Intent in=new Intent(OTPScreen.this,Home.class);
        startActivity(in);
        finish();
    }
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Intent in=new Intent(OTPScreen.this,Home.class);
                startActivity(in);
                finish();
            }
        }
    };
}
