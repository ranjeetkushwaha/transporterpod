package com.dalmia.srivastavsamarth.transporterpod;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Srivastav.Samarth on 7/3/2017.
 */

public class TransporterPOD {
    @SerializedName("customer")
    public String customer;
    @SerializedName("invoice")
    public String invoice;
    @SerializedName("inv_id")
    public String inv_id;
    @SerializedName("D_Del")
    public String D_Del;
    @SerializedName("Quantity")
    public String Quantity;
    @SerializedName("Del_lat")
    public float Del_lat;
    @SerializedName("Del_long")
    public float Del_long;
    @SerializedName("stat")
    public boolean stat;

   public String getInv_id() {
        return inv_id;
    }

    public void setInv_id(String inv_id) {
        this.inv_id = inv_id;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getD_Del() {
        return D_Del;
    }

    public void setD_Del(String d_Del) {
        D_Del = d_Del;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public float getDel_lat() {
        return Del_lat;
    }

    public void setDel_lat(float del_lat) {
        Del_lat = del_lat;
    }

    public float getDel_long() {
        return Del_long;
    }

    public void setDel_long(float del_long) {
        Del_long = del_long;
    }

    public boolean isStat() {
        return stat;
    }

    public void setStat(boolean stat) {
        this.stat = stat;
    }


}
