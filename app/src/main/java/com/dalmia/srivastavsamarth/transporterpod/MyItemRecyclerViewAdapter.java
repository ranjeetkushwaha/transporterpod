package com.dalmia.srivastavsamarth.transporterpod;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;



import java.util.List;


public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private List<TransporterPOD> mValues;
    private List<TransporterPOD> newval;
    private Context context;
    public TextView invoice,date,cust,quant;
    public ConstraintLayout l;
    Button b;

    public MyItemRecyclerViewAdapter(Context context, List<TransporterPOD> itemList) {
        mValues = itemList;

        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_all, parent, false);
        invoice = (TextView) view.findViewById(R.id.invoicelabel1);
        date = (TextView) view.findViewById(R.id.Date1);
        cust = (TextView) view.findViewById(R.id.cusnamlab1);
        quant = (TextView) view.findViewById(R.id.quant1);
        l= (ConstraintLayout) view.findViewById(R.id.con_lay);
        b =(Button) view.findViewById(R.id.button6);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

    invoice.setText("invoice: " + mValues.get(position).getInvoice());
    date.setText("Date: " + mValues.get(position).getD_Del());
    cust.setText("Customer Name: " + mValues.get(position).getCustomer());
    quant.setText("Quantity: " + mValues.get(position).getQuantity());
    }

    @Override
    public int getItemCount() {
          return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;

        }


    }
}
