/*

package com.dalmia.srivastavsamarth.transporterpod;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Random;
import java.util.concurrent.TimeUnit;


import dalmiabharat.com.epod.R;
import dalmiabharat.com.epod.android.networkRequest.ApiInterface;
import dalmiabharat.com.epod.android.networkRequest.RestCalls;
import dalmiabharat.com.epod.android.networkRequest.RetrofitClient;
import dalmiabharat.com.epod.android.utils.AppPreferences;
import dalmiabharat.com.epod.android.utils.CustomHTTPRequest;
import com.dalmia.srivastavsamarth.transporterpod.Utility;

import static com.google.android.gms.analytics.internal.zzy.r;


\


public class MobileCodeConfirm extends AppCompatActivity implements RestCalls.AdapterUpdateCallback {

    int count=0;
    boolean check=false;
    TextView msgalert;
    EditText codeText;
    private CountDownTimer countDownTimer;
    private boolean timerStarted = false;
    Button submitButton;
    boolean loginFailed = true;
    JSONObject jsonObject;
    String verCode,mobileNo, truckNo;
    //SharedPreferences shared;
    public TextView textView,trucknumber;
    private final long startTime = 50 * 1000;
    private final long interval = 1 * 1000;
    long minutesLeft,secondsLeft;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;
  //  Context context;
    public static final String SO_CONTACT = "socontact";
    public static final String SO_ID = "sid";
    public static final String SYNC_TIME = "synctime";
    public static final String SYNC_FREQ = "syncfreq";
    public static final String SYNC_FREQ_PART = "syncfreqpart";
    public static final String LAST_SYNC_TIME = "lastsynctime";

    Context context = this;
    RestCalls restcalls;
    ApiInterface apiService = RetrofitClient.getClient().create(ApiInterface.class);
    int OTPcount=0;
    Utility checkutil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_code_confirm);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        restcalls = new RestCalls(this);
        restcalls.updateAdapter(this);
        context = getApplicationContext();
        checkutil=new Utility (context);
        Intent intent = getIntent();
        verCode=intent.getStringExtra("code");
        mobileNo=intent.getStringExtra("mobileNo");
        truckNo=intent.getStringExtra("truckNo");

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        textView = (TextView) findViewById(R.id.textView);
        trucknumber= (TextView) findViewById(R.id.trucknumber);
        trucknumber.setText("Truck Number: "+truckNo);


        countDownTimer = new CountDownTimer(startTime, interval) {
            @Override
            public void onFinish() {
                if(loginFailed){
                    textView.setText("Time's up!");
                    count = 0;
                    countDownTimer.cancel();
                    timerStarted = false;
                    codeText.setText("");
                    submitButton.setEnabled(true);
                    if(OTPcount==1)
                    {
                        Intent i=new Intent(MobileCodeConfirm.this, MobileCodeFailedActivity.class);
                        startActivity(i);
                        finish();
                    }
                    if(OTPcount==0)
                    {
                        OTPcount=1;
                        submitButton.setText("Resend OTP");
                    }
                }
            }

            @Override
            public void onTick(long millisUntilFinished) {
                textView.setText(""+String.format("%d : %02d ",  TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -  TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                minutesLeft = TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished);
                secondsLeft = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -  TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
            }
        };
        //
        textView.setText(textView.getText() + String.valueOf(startTime/1000));
        if (!timerStarted) {
            countDownTimer.start();
            timerStarted = true;
        }
        codeText = (EditText) findViewById(R.id.smsCode);
        codeText.addTextChangedListener(textWatcher);
        msgalert=(TextView) findViewById(R.id.msg);
        submitButton = (Button) findViewById(R.id.btnVerify);
       // submitButton.setEnabled(false);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(OTPcount==1 && submitButton.getText().toString().equalsIgnoreCase("Resend OTP"))
                {
                    textView.setText(textView.getText() + String.valueOf(startTime/1000));
                    if (!timerStarted) {
                        countDownTimer.start();
                        timerStarted = true;
                    }
                    submitButton.setText("Verify");
                    Random r = new Random();
                    int  i1 = r.nextInt(999999 - 100000) + 100000;
                    verCode=Integer.toString(i1);
                    CustomHTTPRequest.LoginResendOTPCode("" + i1, "91" + mobileNo,context);
                    //submitButton.setEnabled(false);
                }
                else
                {
                    if(codeText.getText().toString().length()>0)
                    {
                        if(codeText.getText().toString().equalsIgnoreCase(verCode))
                            verifySms();
                    }

                }
            }
        });
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            checkRequiredFields();
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    private  void checkRequiredFields() {

        submitButton = (Button) findViewById(R.id.btnVerify);
        codeText = (EditText) findViewById(R.id.smsCode);

        if(codeText.getText().toString().equals(verCode)) {
            check=true;
            submitButton.setEnabled(true);

        }
        else if(codeText.length()!=6 || codeText.equals(""))
        {
            //submitButton.setEnabled(false);
        }

        else
        {
            check=false;
            count++;
            //submitButton.setEnabled(false);

        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (secondsLeft == 1) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onPause() {
        if (secondsLeft == 1) {
            count = 0;
            countDownTimer.cancel();
            timerStarted = false;
            codeText.setText("");
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (secondsLeft == 1) {
            countDownTimer.cancel();
            timerStarted = false;
        }
        super.onStop();
    }
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                if(message!=null && message.length()>0) {
                    codeText.setText(message);
                    restcalls.attemptLogin(truckNo.toString(), mobileNo.toString());
                }
            }
        }
    };
    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }



    public void verifySms (){
        if(checkutil.isOnline()) {
            loginFailed = false;
            countDownTimer.onFinish();
            RestCalls restCalls = new RestCalls(context);
            //   restCalls.updateAdapter(this);
            restcalls.attemptLogin(truckNo.toString(), mobileNo.toString());
        }
        else
        {
            checkutil.createAlertDialog("Please check your internet connection!");
        }

    }

    public void notifyAdapter(Object data) {

        if (AppPreferences.getAppInstance(MobileCodeConfirm.this).isLoggedIn()) {
            startActivity(new Intent(MobileCodeConfirm.this, Home.class));
            finish();
        } else {

        }
        if (data != null) {
            Toast.makeText(MobileCodeConfirm.this, data.toString(), Toast.LENGTH_LONG).show();
            AppPreferences.getAppInstance(MobileCodeConfirm.this).setLoginStatus(true);
            startActivity(new Intent(MobileCodeConfirm.this, Home.class));
            count = 0;
            countDownTimer.cancel();
            timerStarted = false;
            codeText.setText("");
            finish();
        }
        else{
            Toast.makeText(MobileCodeConfirm.this, "No data Found or Login Unsuccessful", Toast.LENGTH_LONG).show();
            AppPreferences.getAppInstance(MobileCodeConfirm.this).setLoginStatus(false);
            startActivity(new Intent(MobileCodeConfirm.this, Login.class));
        }
    }
}

*/
