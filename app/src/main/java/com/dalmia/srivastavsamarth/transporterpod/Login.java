package com.dalmia.srivastavsamarth.transporterpod;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
/*
import org.json.JSONObject;
import org.json.JSONException;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.AsyncHttpResponseHandler;
*/
import android.app.ProgressDialog;
import android.widget.TextView;

import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import java.io.UnsupportedEncodingException;

import static com.loopj.android.http.AsyncHttpClient.log;


public class Login extends AppCompatActivity {

    EditText t1,t2;
    TextView errorMsg;
    ProgressDialog prgDialogue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        t1=(EditText)findViewById(R.id.transCode);
        t2=(EditText)findViewById(R.id.transPhone);
        errorMsg=(TextView) findViewById(R.id.login_error);
        prgDialogue=new ProgressDialog(this);
        prgDialogue.setMessage("Please wait...");
        prgDialogue.setCancelable(false);
    }

   public void loginButton(View view)
    {
        RequestParams params=new RequestParams();
        String s=t2.getText().toString();
        String p=t1.getText().toString();
        if(Utility.isNotNull(s)&&Utility.isNotNull(p))
        {
            if(Utility.validateno(s)) {
                params.put("tr_code",p);
                params.put("tr_mob",s);
                invokeWS(params);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"please enter a valid mobile number!",Toast.LENGTH_SHORT).show();
                t2.setText("");
                t1.setText("");
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Either one or both fields are empty above!",Toast.LENGTH_SHORT).show();
        }

    }

    public void invokeWS(RequestParams requestParams)
    {

        prgDialogue.show();
        AsyncHttpClient client=new AsyncHttpClient();
        client.get("http://10.10.28.127:8081/TransporterPOD/login/dologin", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                prgDialogue.hide();
                String response=null;
                try {
                    response=new String(responseBody,"UTF-8");
                }
                catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                }
                try {
                    JSONObject obj=new JSONObject(response);
                    if(obj.getBoolean("status"))
                    {

                        Toast.makeText(getApplicationContext(),"wait for the OTP to verify this no.",Toast.LENGTH_LONG).show();
                        Intent in = new Intent(Login.this, OTPScreen.class);
                        startActivity(in);
                        finish();
                    }
                    else
                    {
                        errorMsg.setText(obj.getString("error_msg"));
                    }
                }

                catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                prgDialogue.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]"+ statusCode, Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
